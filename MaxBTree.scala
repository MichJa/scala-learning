
import scala.math.max, scala.math.min, scala.util.Random

class BTreeNode(v: Int, l: BTreeNode, r: BTreeNode) {
	println("Node value " + v + " created.")
	def value = v	
	def leftChild = l
	def rightChild = r
}

object MaxBTree {
	val random = new Random()
	
	// not really n nodes, just some number
	def initBTree(n: Int): BTreeNode = {
		if (n > 0)
			new BTreeNode(random.nextInt(100), initBTree(n/2-1), initBTree(n/2-1))
		else
			new BTreeNode(random.nextInt(100), null, null)
	}
	
	def maxNode(parent: BTreeNode): Int = {
		val left: Int = if(parent.leftChild != null) maxNode(parent.leftChild) else -1
		val right: Int = if(parent.rightChild != null) maxNode(parent.rightChild) else -1
		val maxChild = max(left, right)
		max(maxChild, parent.value)
	}

	def main(args: Array[String]) {
		val tree = initBTree(args(0).toInt)
		val maxValueInTree = maxNode(tree)
		println("Maximum: " + maxValueInTree)
	}
}
